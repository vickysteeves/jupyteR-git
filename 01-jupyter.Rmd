# Literate Programming with JupyteR {#jupyter-notebooks}

<img style="float: left; padding-right:0.5em;" src="img/jupyter.png"><span style="color:#e46e2e;">**Jupyter Notebooks**</span> are another tool we can use to interweave code and narrative to write more complete records of our work. Jupyter actually stands for Julia, Python, and R! A Jupyter Notebook document is actually a fancy JSON document. It contains an ordered list of input/output cells which can contain code, text (using Markdown), mathematics, plots, rich media, and uses the ".ipynb" extension. Notebook documents are both human-readable documents containing the analysis description and the results (figures, tables, etc..) as well as executable documents which can be run. 

A *kernel* is a process running an interactive session. When using IPython, this kernel is a Python process. There are kernels in many languages other than Python. In Jupyter, notebooks and kernels are **strongly separated**. A notebook is a file, whereas a kernel is a process. 

The kernel receives snippets of code from the Notebook interface, executes them, and sends the outputs and possible errors back to the Notebook interface. A notebook is persistent (it's a file), whereas a kernel may be closed at the end of an interactive session and it is therefore not persistent. When a notebook is re-opened, it needs to be re-executed.

You can use A TON of languages with Jupyter Notebooks, provided you can get the kernel (which is waaay harder for the proprietary languages): Python (of course), R, SageMath, Bash, Octave, Julia, Haskell, Ruby, JavaScript, Scala, PHP, GO, and [many more](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels). When installed as Jupyter kernels each language becomes accessible in the same way, using the same notebook interface. You can't mix programming languages in one notebook, however. One notebook = one language. 

Jupyter notebooks work with basically two parts:

1. Web Application (notebook + kernel)
  + In-browser editing for code with auto-syntax highlighting, indentation, tab completion/introspection
  + In-browser code execution, with results attached to the code that generated them
  + Display results of computation in rich media (LaTeX, HTML, SVG, etc.)
2. Notebook (document)
  + A complete computation record of a session, interweaving executable code with text, maths, and rich representations of objects
  + Can export to LaTeX, PDF, slideshows, etc. or available from a public URL that renders it as a static webpage

Jupyter notebooks are what some call an "executable paper" because of the functions outlined above. However, the same problems occur where computing environments differ, so you have to take extra steps to make these reproducible, which we'll see in the next module.

**Examples**:

+ [Analyzing Whale Tracks](https://nbviewer.jupyter.org/github/robertodealmeida/notebooks/blob/master/earth_day_data_challenge/Analyzing%20whale%20tracks.ipynb), by Roberto De Almeida
+ [A Reconstruction of 538 2012 Election Model](https://nbviewer.jupyter.org/github/jseabold/538model/blob/master/silver_model.ipynb), by Skipper Seabold
+ [Visual White Matter](https://github.com/arokem/visual-white-matter) by Ariel Rokem

## Getting started
You should all have access to my NYU JupyterHub instance here:  [https://rdm.apps.brc.stern.nyu.edu/](https://rdm.apps.brc.stern.nyu.edu/). You should be able to log-in with your NYU netID and password. Because we signed our own certificate (lol), you might get a warning from your browser saying that it's an insecure conenction. Just click "Andvanced" > "Add exception" and proceed to the site.

Once you are in our class interface, you should see something like this:
![Class JupyterHub interface](img/jupyterhub.png)

The dashboard contains several tabs:

+ **Files** shows all files and notebooks in the current directory.
+ **Running** shows all kernels currently running on your computer.
+ **Clusters** lets you launch kernels for parallel computing.

If you want to run Jupyter notebooks locally, I recommend installing via [Anaconda](https://www.anaconda.com/download/) -- it has a lot of Python libraries pre-installed that are great for research, like `pandas`, `scipy`, `numpy`, `matplotlib`, etc. You will need to add the R kernel separately -- you can find [the instructions](https://irkernel.github.io/installation/) on the maintainer's website. From there you will be able to add all the R packages that you need, like `ggplot2` and other packages from the tidyverse.

So, let's get started!

Click that button on the top right corner that says 'New' and select R. A blank notebook should then launch:
![Blank notebook](img/blank-ipynb.png)

Then you get your brand-new notebook!

![The layout of the notebook |  [source](https://nbviewer.jupyter.org/github/ipython-books/minibook-2nd-code/blob/master/chapter1/13-nbui.ipynb)](img/notebook-overview.png)

The main components of the interface, from top to bottom:

+ **The notebook name**: you can change by clicking on it. This is also the name of the `.ipynb` file.
+ The **menu bar** gives you access to several actions pertaining to the notebook (like saving it!) and the kernel (like restarting it!)
+ To the right of the menu bar is the **Kernel** name. You can change the kernel language of your notebook from the Kernel menu.
+ The **toolbar** contains icons for common actions. In particular, the dropdown menu showing Code lets you change the type of a cell.
+ Below is the actual Notebook. It consists of a linear list of cells. You should only run your notebook from top to bottom -- ONLY.

First, let's change the name from 'Untitled' to something useful. There's a running gag in the data science community that at any given time, a data scientist has a series of Jupyter notebooks that look like:

~~~
Untitled.ipynb
Untitled1.ipnyb
...
Untitled31.ipynb
~~~

This obviously gives me, the data management librarian, hives. SO:

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 1:**

1. Change the name of your jupyter notebook.
3. Raise your hand to show you've finished!

</div><br/>

You can see the notebook itself consists of cells -- we have one to start out with. Once we double click on a cell, we are in *insert mode*. This means that we are able to edit the cells, just as you would if this were a word document. We can tell that we are in insert mode because of the green border around the cell.

When we're in a Jupyter notebook, there are some useful shortcuts to get us started:

+ `esc` in highlighted cell to toggle command options:
    - `esc + l` - show line numbers (that's a lowercase L)
    - `esc + m` - format cell as Markdown cell
    - `esc + a` - insert cell above current cell
    - `esc + b` - insert cell below current cell
+ `shift + enter` - run an active cell
+ `command + z` - undo (macOS)
+ `control + z` - undo (Windows)

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 2:**

1. Add line numbers to your jupyter notebook.
2. Insert a cell below the current cell you're on.
3. Raise your hand to show you've finished!

</div><br/>

## Working in our notebook

Ok, so let's edit a cell! Once you've double clicked on the first cell, let's write some text about what this notebook is going to do. The cells default to code, so we need to press `esc + m` to change the cells to markdown. This is how we'll write our narratives in between code cells! Since in our last challenge we added an extra cell, I made them both markdown: 

![Writing markdown cells in Jupyter](img/cell-markdown.png)

Then, let's add a cell beneath our markdown cells (`ctrl + b`) -- in this next cell, we can begin our analysis and enter some basic R code! In R, we assign values by typing out the `<-`. We use this for objects, for DataFrames -- for everything that needs an assignment. So, let's make a variable in a new code cell:

~~~
cool_num <- 100    # assigns the object a value and a name to call it by
(cool_num <- 100)  # but putting parenthesis around the call prints the value of `cool_num`
cool_num           # and so does typing the name of the object
~~~

Then click `shift + enter` to run the cell, or click the `> Run` button on the toolbar! You should see the output of your code immediately below the cell that generated it:

![The results from code execute beneath the cell that generates them](img/cell-execution.png)

We are going to some basic code & plots in our notebook, so we have something pretty to export at the end of the session! To do that, we first need to import the relevant R libraries for our plotting and data analysis:

~~~
library("dplyr")
library("leaflet")
library("ggplot2")
library("plyr")
~~~

Ideally, this comes first in the notebook, but this is an intro class, so some concepts need to come first! 

So in a jupyter notebook, each cell should represent a conceptually different process. You can separate code cells with markdown explaining what it does. 

![Editing our new cells](img/cell-execution2.png)

Jupyter notebooks, RMarkdown files, and all other computational notebooks are meant to be run top-to-bottom. If you get **nothing else** out of this talk than the knowledge that your notebooks **must** run top-to-bottom, I'd honestly consider it a win.

Because notebooks are run top-to-bottom, we can use variables from previous cells! So in the next code cell we are going to make a variable based on our first variable `cool_num`:

~~~
other_num <- 2.2 * cool_num # doing some arithmetic and assigning it to a new object
other_num
~~~

Execute the cell, and you should see `220`. If it doesn't work, make sure you ran the previous cell and actually made the variable.

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 3:**

1. Write some text in a new markdown cell explaining what you're about to do in the code chunk directly below it.
2. Add an R code chunk, and assign an object a value with a name.
3. Print out your object.
4. Raise your hand to show you've finished!
</div><br/>

One of the main reasons people use R is the bevy of `function`s. Functions are built into R and extended with R packages. Functions help automate more complicated sets of commands. We'll only use predefined functions, but you can also define your own as your R proficiency grows!

A function usually gets one or more inputs called `arguments`. Functions often (but not always) return a value. 

We'll take a look at the function `sqrt()`. The input (the argument) must be a number, and the return value (in fact, the output) is the square root of that number. Executing a function (‘running it’) is called `calling the function`. Let's find the square root of our variable `other_num` in a new code cell:

~~~
sqrt(other_num)
~~~

The return 'value' of a function doesn't have to be a number or even a single item! It can be a whole dataset. We'll see that later, if time allows.

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 4:**

1. Type `library(help = "base")` in the console in R. If you need more information, use `?functionName` substituting functionName for the actual name of the function. 
2. Find a function that gives us back a numerical or Boolean value.
3. Add an R code chunk, and call the function from it using one of our previous objects.
4. Raise your hand to show you've finished!
</div><br/>

**Looking at data types**

The first main data type in R is a `vector`. A vector is composed by a series of values, which can be either numbers or characters.

~~~
vec1 <- c(4.6, 4.8, 5.2, 6.3, 6.8, 7.1, 7.2, 7.4, 7.5, 8.6)    
vec2 <- c('UT', 'IA', 'MN', 'AL', 'WI', 'MN', 'OH', 'IA', 'NY', 'IA')

vec1
vec2
~~~

You can take the values from vectors and put them into another wholly different vector! You'll notice I use `1`, `2`, etc. instead of the actual numbers in my vector. That's because I have to give the *location* of the data point inside the vector, not the actual data point itself. R is one of the few languages that start their index at 1 (most use 0).

~~~
more_vec <- vec1[c(1, 2, 3, 2, 1, 4)]
more_vec
~~~
You can also subset vectors to find data within them, using the `==` which tests for equality (are the two values the same??). Another useful subset is to use `<` and `>` to find values greater than or less than some measure within the subset.

~~~
vec1[vec1 < 2 | vec1 > 5]
vec2[vec2 == "MN" | vec2 == "OH"]
~~~

R was made for data analysis, so it stands to reasons that there is functionality to deal with the everyday messiness of data. This includes dealing with missing data too, which we see a lot in everyday life. Take this vector, `cat_toys`, which has one missing value. When I got to run some basic math, it fails:

~~~
cat_toys <- c(20, 14, 4, NA, 60)
mean(cat_toys)
max(cat_toys)

expo_vec <- vec1*5 # you can even do math right on vectors!
expo_vec
~~~

However, R has a way to deal with that! You can add the argument `na.rm=TRUE` to calculate the result while ignoring the missing values.

~~~
mean(cat_toys, na.rm = TRUE)
max(cat_toys, na.rm = TRUE)
~~~

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 5:**

1. Write a code chunk that creates this vector: `heights <- c(63, 69, 60, 65, NA, 68, 61, 70, 61, 59, 64, 69, 63, 63, NA, 72, 65, 64, 70, 63, 65)`
2. Use R to figure out how many people in the set are taller than 67 inches.
3. Use the function median() to calculate the median of the heights vector.
4. Raise your hand to show you've finished!
</div><br/>

**However** the most popular data type in R for sure are Data frames. This is what most folks use most tabular data, statistics, and plotting. Data frames are made of vectors.

A data frame can be created by hand, but most commonly they are generated by the functions read.csv() or read.table(); in other words, when importing spreadsheets from your hard drive or the web.

A data frame is essentially a table where the columns are vectors that all have the same length. Because columns are vectors, each column must contain a single type of data (e.g., characters, integers, factors). For example, here is a figure depicting a data frame comprising a numeric, a character, and a logical vector.

![Data frame image; from data carpentry ecology lesson](img/data-frames.png)

**Let's read in some data** 

Please download this data

~~~
uni <- read.csv(url("https://osf.io/qxgvn/download"))
~~~

Now we have a new data frame `uni`! If we want to see the first few rows of data to verify it's right, we can use the `head()` function and if we want to see the first column, we call the data frame with the location of what we want -- it goes `dataFrame[col,row]`:

~~~
head(uni) # see first six rows

head(uni[1])  # see the first column
~~~

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 6:**

1. Load in our cleaned data using `read.csv()`
2. Find the first cell in the 1st column using R.
3. Find the first three cells in the 4th column using R.
4. Raise your hand to show you've finished!
</div><br/>

**Useful data frame functions**

Size:

+ dim(uni) - returns a vector with the number of rows in the first element, and the number of columns as the second element (the dimensions of the object)
+ nrow(uni) - returns the number of rows
+ ncol(uni) - returns the number of columns

Content:

+ head(uni) - shows the first 6 rows
+ tail(uni) - shows the last 6 rows

Names:

+ names(uni) - returns the column names (synonym of colnames() for data.frame objects)

Summary:

+ summary(uni) - summary statistics for each column

We can also subset our dataframes by putting them into vectors, like this chunk which takes all the names of univerisites and puts them into another dataframe:

~~~
unis <- uni["universities"] # can also do `uni[1]`, I passed it name of column
head(unis)
~~~

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 7:**

1. Subset `uni` to create a new dataframe called `uni_money` that contains the university name and the endowment.
2. Compute some summary statistics about the levels of endowments per university.
4. Raise your hand to show you've finished!
</div><br/>

~~~
uni_money<- subset(uni, select=c("universities", "endowment")) # make a new subset
head(uni_money) # makes ure it looks right
~~~

We are going to deduplicate the data and sum the rows to try to get the total endowment for each university, then display the head:

~~~
uni_money$endowment <- as.numeric(as.character(uni_money$endowment)) # make the endowment column a number
dedupe_unimoney<-ddply(uni_money,.(universities),function(x)
data.frame(universities=x$universities[1],endowment=sum(x$endowment)))

head_dedupedMoney <- head(dedupe_unimoney) # get the first 6 rows of our newly deduped and summed rows
head_dedupedMoney
~~~

Now that we have some clean, relevant information to use, let's plot it! I mainly use R for plotting purposes myself. I think the range and options are vastly superior to other data viz packages out there. I can even get plots with DPIs enough for print!

For now, let's do a basic histogram:

~~~
p <- ggplot(head_dedupedMoney, aes(x = universities, y = endowment))  + geom_histogram(stat="identity")
p
~~~

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 8:**

1. Pick another relevant plot using `ggplot`'s functions and plot the data a different way.
2. BONUS: add some color!
4. Raise your hand to show you've finished!
</div><br/>

We can also do some mapping in R and make it interactive with `leaftlet`! First we need to make *another* subset, with just the relevant information -- the universities and their geographic locations. 

~~~
plot<- subset(uni, select=c("universities", "lat", "long")) # make a new subset for map
uni_plot <- unique(plot[,]) # deduplicate the data! 

head(uni_plot) # makes ure it looks right
~~~

Ok, let's map!

~~~
m <- leaflet(uni_plot) %>% 
addProviderTiles(providers$CartoDB.Positron)
m %>% setView(-72.690940, 41.651426, zoom = 8)
~~~

Now that we made the base map, we can add markers to it:

~~~
m %>% addMarkers(~long, ~lat, popup = ~as.character(universities), label = ~as.character(universities)) 
~~~

## Saving & Exporting
Jupyter autosaves your notebook, but just to be sure I always save after anything important. You can go to `File > Save & Checkpoint` or press `ctrl + s`.

I also recommend `Save & Checkpoint` because you can revert back to previous checkpoints in case something breaks! `File > Revert to Checkpoint` and then you select the checkpoint you want to go back to, which is labeled with date/time.

One of the best things about Jupyter notebook is that you can export the notebook in a variety of formats:

+ PDF - it's executed via LaTex but you don't have to touch it
+ HTML - a static rendering for the web
+ Python - a python script 
+ LaTex - if you want the raw LaTex to apply a 
+ reST - reStructuredText, another text format
+ Markdown - like how I am writing this book!

A lot of folks I know actually write their blog posts in Jupyter notebooks, then export it for their website!

<div style="border: 2px solid black; text-align:center;">
**CHALLENGE 10:**

1. Export your Jupyter notebook as a PDF
2. How does it look? Would you write a paper in Jupyter?
3. Raise your hand to show you've finished!

</div><br/>

### Moving a Notebook to the Web
You might have noticed some of our 

+ [NBViewer](https://nbviewer.jupyter.org/): for static rendering of notebook files
    + Put notebook (.ipynb) file on the web (e.g. Github, Gitlab...somewhere so that URL is  http://NAME-OF-NOTEBOOK.ipynb
    + Enter the URL into NBViewer.
    + Click Go! (check out example here) 

+ [Binder](): for **interactive** rendering of notebook files!
    + You need a repository of jupyter notebooks plus a `requirements.txt` file that lists all the python libraries and version of python you use for your notebooks.
    + Enter your repository information (a URL to a GitHub repo with Jupyter Notebooks) in Binder
    + Binder builds a Docker image of your repository using a `requirements.txt` file from the repository.
    + Binder builts the notebook environment for you, and lets you interact with your notebooks in a live environment!
    + You can also get a reusable link and badge for your live repository that you can easily share with others, like so: https://hub.mybinder.org/user/tiesdekok-learnpythonforresearch-zjynb3wo/tree 

+ [GitHub Pages](https://pages.github.com/) or [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/):
    + You can use a static site generator like Nikola or Jekyll to blog with Jupyter notebooks!
    + If you want a more simple approach, just export your notebook as `index.html`, put it in a repository, and configure the settings in your repository for pages (links above for tutorial).
    + Example: [GitLab pages](https://vickysteeves.gitlab.io/Stories-Code-Data-Py/)

## Further Reading
+ An Introduction to R: [cran.r-project.org/doc/manuals/R-intro.html](https://cran.r-project.org/doc/manuals/R-intro.html)
+ Quick-R Tutorial: [statmethods.net/r-tutorial/index.html](https://www.statmethods.net/r-tutorial/index.html)
+ R Bloggers - Tutorials for Learning: [r-bloggers.com/how-to-learn-r-2/](https://www.r-bloggers.com/how-to-learn-r-2/)
+ Here, here! Why I love the Here package: [github.com/jennybc/here_here](https://github.com/jennybc/here_here)
+ Gallery of Jupyter notebooks, by language + domain: [github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks](https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks)
+ Exploring data in Jupyter: [nbviewer.jupyter.org/github/ipython-books/minibook-2nd-code/blob/master/chapter2/21-exploring.ipynb](https://nbviewer.jupyter.org/github/ipython-books/minibook-2nd-code/blob/master/chapter2/21-exploring.ipynb)
+ Jupyter Notebook docs: [jupyter-notebook.readthedocs.io/en/stable/](https://jupyter-notebook.readthedocs.io/en/stable/)
+ Excellent Tutorial by Justin Bois at Caltech: [bebi103.caltech.edu.s3-website-us-east-1.amazonaws.com/2015/tutorials/t0b_intro_to_jupyter_notebooks.html#Best-practices-for-code-cells](http://bebi103.caltech.edu.s3-website-us-east-1.amazonaws.com/2015/tutorials/t0b_intro_to_jupyter_notebooks.html#Best-practices-for-code-cells)
+ Jupyter Notebook Tutorial: Introduction, Setup, and Walkthrough: [youtube.com/watch?v=HW29067qVWk](https://www.youtube.com/watch?v=HW29067qVWk)
+ Jupyter Notebook for Beginners: A Tutorial: [dataquest.io/blog/jupyter-notebook-tutorial/](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)